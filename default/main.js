var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleRepairer = require('role.repairer');
var roleScavenger = require('role.scavenger');
var logicTower = require('logic.tower');
var roleMaintainer = require('role.maintainer');

module.exports.loop = function () {

    global.getStats = function() {
        for(var name in Game.rooms) {
            console.log('Room "'+name+'" has '+Game.rooms[name].energyAvailable+ '/' + Game.rooms[name].energyCapacityAvailable +' energy');
        }
        console.log('Harvesters: ' + harvesters.length + '/' + harvestersPop);
        console.log('Builders: ' + builders.length + '/' + buildersPop);
        console.log('Upgraders: ' + upgraders.length + '/' + upgradersPop);
        console.log('Repairers: ' + repairers.length + '/' + repairersPop);
        console.log('Scavengers: ' + scavengers.length + '/' + scavengersPop);
        console.log('Maintainers: ' + maintainers.length + '/' + maintainersPop);
    }

    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name); //testing jetbrains gitlab keys
        }
    }

    var harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
    let harvestersPop = 3;
    var builders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder');
    let buildersPop = 0/*1*/;
    var upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader');
    let upgradersPop = 2/*2*/;
    var repairers = _.filter(Game.creeps, (creep) => creep.memory.role == 'repairer');
    let repairersPop = 2/*3*/;
    var scavengers = _.filter(Game.creeps, (creep) => creep.memory.role == 'scavenger');
    let scavengersPop = 1;
    var maintainers = _.filter(Game.creeps, (creep) => creep.memory.role == 'maintainer');
    let maintainersPop = 2;

    if(harvesters.length < harvestersPop) {
        var newName = 'HarvesterV3_' + Game.time;
        console.log('Spawning new harvester: ' + newName);
        /*V3*/Game.spawns['Spawn1'].spawnCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE], newName,
            {memory: {role: 'harvester'}});
        ///*V2*/ Game.spawns['Spawn1'].spawnCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE], newName,
        //     {memory: {role: 'harvester'}});
    }

    if(builders.length < buildersPop) {
        var newName = 'BuilderV2_' + Game.time;
        console.log('Spawning new builder: ' + newName);
        Game.spawns['Spawn1'].spawnCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE], newName,
            {memory: {role: 'builder'}});
    }

    if(upgraders.length < upgradersPop) {
        var newName = 'UpgraderV3_' + Game.time;
        console.log('Spawning new upgrader: ' + newName);
        /*V3*/Game.spawns['Spawn1'].spawnCreep([WORK,WORK,WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE], newName,
            {memory: {role: 'upgrader'}});
        ///*V2*/Game.spawns['Spawn1'].spawnCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE], newName,
        //{memory: {role: 'upgrader'}});
    }

    if(repairers.length < repairersPop) {
        var newName = 'RepairerV2_' + Game.time;
        console.log('Spawning new repairer: ' + newName);
        Game.spawns['Spawn1'].spawnCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE], newName,
            {memory: {role: 'repairer'}});
    }

    if(scavengers.length < scavengersPop) {
        var newName = 'ScavengerV2_' + Game.time;
        console.log('Spawning new scavenger: ' + newName);
        Game.spawns['Spawn1'].spawnCreep([WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE], newName,
            {memory: {role: 'scavenger'}});
    }
    if(maintainers.length < maintainersPop) {
        var newName = 'MaintainersV4_' + Game.time;
        console.log('Spawning new maintainer: ' + newName);
        Game.spawns['Spawn1'].spawnCreep([WORK,WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE], newName,
            {memory: {role: 'maintainer'}});
    }

    if(Game.spawns['Spawn1'].spawning) {
        var spawningCreep = Game.creeps[Game.spawns['Spawn1'].spawning.name];
        Game.spawns['Spawn1'].room.visual.text(
            '🛠️' + spawningCreep.memory.role,
            Game.spawns['Spawn1'].pos.x + 1,
            Game.spawns['Spawn1'].pos.y,
            {align: 'left', opacity: 0.8});
    }

    logicTower.run();

    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
            //roleHarvester.run(creep);
        }
        if(creep.memory.role == 'builder') {
            //roleRepairer.run(creep);
            //roleHarvester.run(creep);
            roleBuilder.run(creep);
        }
        if(creep.memory.role == 'repairer') {
            roleRepairer.run(creep);
        }
        if(creep.memory.role == 'scavenger') {
            roleScavenger.run(creep);
        }
        if(creep.memory.role == 'maintainer') {
            roleBuilder.run(creep);
            roleMaintainer.run(creep);
        }
    }
}

/* module.export.energyCurrent = function energyCurrent() {
        for(var name in Game.rooms) {
            console.log('Room "'+name+'" has '+Game.rooms[name].energyAvailable+' energy');
        }
    } */