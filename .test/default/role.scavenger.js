
var roleScavenger = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.memory.scavenging && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.scavenging = false;
            creep.say('🔄 scavenge');
        }
        if(!creep.memory.scavenging && creep.store.getFreeCapacity() == 0) {
            creep.memory.scavenging = true;
            creep.say('✨ fill');
        }

        if(!creep.memory.scavenging) {
            var targetDropped = creep.room.find(FIND_DROPPED_RESOURCES);
            var targetTombstone = creep.room.find(FIND_TOMBSTONES, {
                filter: (tombstone) => {
                    return tombstone.store.getUsedCapacity(RESOURCE_ENERGY) > 0;
                }
            });
            if(targetDropped.length > 0) {
                creep.moveTo(targetDropped[0], {visualizePathStyle: {stroke: '#ffaa00'}});
                creep.pickup(targetDropped[0]);
            } else if(targetTombstone.length > 0) {
                if(creep.withdraw(targetTombstone[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targetTombstone[0], {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            }
        }
        else  {
            var targetBasic = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_EXTENSION ||
                        structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_TOWER) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
                }
            });
            var targetsCont = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_CONTAINER) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;

                }
            });
            var targetsStore = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_STORAGE) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0;

                }
            });
            var targets = targetBasic.concat(targetsCont, targetsStore);
            if(targets.length > 0) {
                if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});

                }
            }
        }
    }
};

module.exports = roleScavenger;