
var logicTower = {

    run: function() {
        for (var roomName in Game.rooms) {
            var towers = Game.rooms[roomName].find(
                FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
            function attackClosestHostile() {
                for (var i = 0; i < towers.length; i++) {
                    var closestHostile = towers[i].pos.findClosestByRange(FIND_HOSTILE_CREEPS);
                    if (closestHostile) {
                        var username = closestHostile.owner.username;
                        Game.notify(`User ${username} spotted in room ${roomName}`);
                        towers[i].attack(closestHostile);
                    }
                }
            }
            function repairClosestStructure() {
                for (var i = 0; i < towers.length; i++) {
                    var damagedStructures = Game.rooms[roomName].find(FIND_STRUCTURES, {
                        filter: (structure) => structure.hits < structure.hitsMax &&
                            structure.structureType != STRUCTURE_WALL &&
                            structure.structureType != STRUCTURE_RAMPART
                    });
                    var damagedWall = Game.rooms[roomName].find(FIND_STRUCTURES, {
                        filter: (structure) => structure.hits < 60000 && (
                            structure.structureType == STRUCTURE_WALL ||
                            structure.structureType == STRUCTURE_RAMPART)
                    });
                    var closestDamagedStructure = towers[i].pos.findClosestByRange(damagedStructures.sort((a,b) => a.hits - b.hits));
                    var closestDamagedWall = towers[i].pos.findClosestByRange(damagedWall.sort((a,b) => a.hits - b.hits));
                    if (closestDamagedStructure) {
                        towers[i].repair(closestDamagedStructure);
                    } else if (closestDamagedWall) {
                        towers[i].repair(closestDamagedWall);
                    }
                }
            }
            // TODO function healCreeps() {}
            var towerEnergyT2 = Game.rooms[roomName].find(
                FIND_MY_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_TOWER) &&
                            structure.store[RESOURCE_ENERGY] > 609;
                    }
                });
            var towerEnergyT1 = Game.rooms[roomName].find(
                FIND_MY_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_TOWER) &&
                            structure.store[RESOURCE_ENERGY] > 309;
                    }
                });
            var towerEnergyT0 = Game.rooms[roomName].find(
                FIND_MY_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_TOWER) &&
                            structure.store[RESOURCE_ENERGY] > 9;
                    }
                });
            //towerEnergyT2.foreach(healCreeps);
            towerEnergyT1.forEach(repairClosestStructure);
            towerEnergyT0.forEach(attackClosestHostile);
        }
    }
};

module.exports = logicTower;