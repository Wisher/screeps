

## TODO

This file will show the current tasks I have planned, and order them into subcategories of importance. Items within each list will likely be unsorted.



### Immediate

- [ ] Make multiple tiers for each Creep type
- [ ] Spawn Creep Tier based on - available energy?
  - IF statement inside the creep spawn code should fix this, e.g. if(Game.rooms.name[name].energyAvailable > xxx) else if > zzz etc
- [x] Make Harvester fill structureTypes in a predefined order e.g. Spawn>Extenstion>Tower>Container
  - Target variable as an array should fix this
- [ ] Make Idle creeps move away from the building they are at.
  - Either make an idle role, if that works. OR add code to their false memory flag IE the flag where they collect resources. At the moment they stop at the resource node/container. Make it so they stand a lil bit away? (try using a flag maybe?)
- [x] adjust upgraders to find closest transferable source ie container/storage instead of the first built.
- [ ] Adjust scavengers to fill Storage else perform repair functions?

### Soon

- [ ] Move commands into separate Module
- [ ] Move Creep Spawns into separate Module
- [x] Scavenger Creep to collect energy from dead Creeps, deposit to same buildings as harvester?
  - [x] give Scavengers logic to move towards tombstones so they can pickup resources faster
  - [ ] find out if buildings leave rubble aswell which also decays, ifso then add logic for that too. Will need to prioritize existing resources before things like tombstones
- [ ] Add IF statement to builder spawn code to check for any construction sites, if NO construction sites, don't spawn
- [ ] Make towers prioritize closest targets, refer to tutorial script. May have to add the logic at the => part
  - [x] prioritize repairing closest structure
  - [x] prioritize attacking closest enemy
    - [ ] **TEST IN SIM**
  - [ ] prioritize healing closest creep
  - [ ] Check if all ~~towers are targetting the same thing~~, maybe each tower is receiving the first instruction.
    - [ ] maybe the capacity of a tower is only being checked on one tower
    - [ ] one of the towers used all it's energy and i didnt see hostile creeps.
- [x] Make towers only repair if they have more than xx energy, 1/3 capacity? (to maintain energy for killing)
- [ ] Add code for towers to heal creeps
- [x] Make maintainer creeps
  - Creeps which take resources from containers/storage to destination, maybe give them other additional tasks. If no additional tasks could rename to filler creeps or something. This will make harvester creeps focus soley on harvesting.
  - merge builders into the maintainer creep setup aswell
    - [ ] should the maintainers refilling towers target by distance AND least capacity? otherwise would they get stuck filling the closest? May need to use sort on storageused for towers
    - [ ] perhapes shouldn't refill towers until their missing storage is greater than the 90%-75% of the creeps carry capacity?
- [x] make all creeps except harvesters draw energy from Storages
- [ ] Defender creeps
  - [ ] 3 variants, melee, ranged siege
    - [ ] Melee would be for attacking enemies, near ramparts? slightly beyond?
    - [ ] Ranged should primarily assist melee? Attack from on/behind ramparts
    - [ ] Siege would be for constructing/repairing ramparts/walls/towers etc.
      - would the same creeps also be used for attacking enemies? Perhaps separate logic for being in an enemy room, would also need logic to attack an enemy room, perhapes an extended period of positive income to fuel an offensive siege or something. Could start off with manual commands to begin an attack/end one. But the final result would need automated logic based on my ideals/playstyle.
- [ ] 

### Distant

- [ ] Is it possible to measure energy use over a time period, could then use that info to determin creep tier spawns etc.
- [ ] Refactor code to execute logic per room, e.g. creeps tiers spawned depends on information for THAT room
- [ ] make logic to determine creep parts based on needs and resources, Maybe make each creep have a % of the total resources, or resources aquired over a period of time. Then give each part a weight for how much it can fill that creeps allotment of resources. With minimums set, this could remove the need for tiers. Will need maths to ensure what is being made is the most efficient for the resource allotment. Should also use maths to determin the resource allotment to each creep type aswell for ensuring functional economy
- [ ] make towers and repair creeps work better together. Will likely need different priorities. Towers repair buildings first? While creeps repair walls ramparts first? Will want creeps to still repair buildings if needed though. Perhapes make a ratio for the creeps to decide on repairing wall/rampart or other
  - [x] Adjusted tower taregetting, they work together better now, but can still use improvement
- [ ] make logic to determin the priority of tower heal/repair/attack, ie change the tier depending on situation?
- [ ] add html links to commands, e.g. make the roomname a link which takes you to that room
- [ ] economy monitoring logic which ensures the energy income is positive, adjusts production/creepnumber/repairamounts/offensive&defensiveCapabilities etc.

