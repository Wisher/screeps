## Welcome to Wishers Screeps Repository!

This repository shows the journey from the very start of my Screeps gameplay. At the time I made this I am a complete beginner, having only just learned functions in JavaScript.

This project is open to be used by others, or for improvements to be suggested by others. Feel free to Fork, request to branch and merge as you please!

I have some recommended tools to help:

- [ScreepsAutocomplete](https://github.com/Garethp/ScreepsAutocomplete): This is a tool which will add auto-complete functionality from the games editor to your personal IDE, works well with WebStorm and VSCode.
- [Live Share](https://github.com/MicrosoftDocs/live-share): The VSCode extention named **`Live Share`** by Microsoft is a great tool if you are editing Screeps scripts with friends.
- Official [Docs](https://docs.screeps.com/) & [API](https://docs.screeps.com/api/)
- [Tutorial Scripts](https://github.com/screeps/tutorial-scripts): A copy of the scripts from the tutorial, great if your just getting started.
- [Emoji](http://unicode.org/emoji/charts/emoji-style.txt): A list of emojis you can use for your screeps messages.

You can watch me play on [Twitch](https://www.twitch.tv/77wisher77/).

Join my [Discord](https://discord.gg/4TFrAKH) to talk about Screeps!



#### Requesting Repository Access

If you would like be a member of the repo for any reason. Email me at `enquiries@aliphaticus.net`, You will also be required to join the [Discord](https://discord.gg/4TFrAKH) for easy communication.



#### Submitting Code

To submit code you will make a branch, commit the code you wish to alter/add, then submit a merge request. Naming conventions should be something like `feature/creep_pathing`, ie, `feature/` then shorthand of what your code achieves. Submit the merge request to the appropriate destination branch.

##### ScreepsAutoComplete setup VSCode

If ScreepsAutoComplete is being troublesome to setup in VSCode, try this:

> 1. Add ScreepsAutocomplete-master to your screeps scripts directory (ingame, open folder)
> 2. Added [_references.js](https://gist.github.com/quonic/d7a7d385c85846027a7ca3dd03a0e985) to ScreepsAutocomplete folder
> 3. Added jsconfig.json to ScreepsAutocomplete folder this is the file with in it:
>    { "compilerOptions": { "target": "ES6" }, "exclude": [ "node_modules" ] }
> 4. Opened a terminal from VSCode and did these two:
>    npm install @types/screeps
>    npm install @types/lodash
> 5. Restarted VSCode